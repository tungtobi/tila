# Bài tập lớn Tila - Phần Scanner 

**Đề bài: Xây dựng trình biên dịch cho ngôn ngữ Tila với văn phạm như file đính kèm.**

## Thông tin học viên
- Họ tên: Nguyễn Tùng Lâm
- MS: 21025095

## Hướng dẫn sử dụng

```
java -jar release/tila-1.0.0.jar <path-to-tila-src-file>
```

## Một số ví dụ

### Ví dụ 1
```
begin
    int x;
    x = 41 - 10;
    print x;
end
```

![](https://i.imgur.com/LyfuQuz.png)

### Ví dụ 2
```
begin
    int x; int y; int z;
    z = 100;
    y = 13 * 4;
    x = (y - z) / (y * z);
    print (x * 56);
end
```

![](https://i.imgur.com/2mZSzWE.png)
![](https://i.imgur.com/BMImVfB.png)

### Ví dụ 3
```
begin
    int x = 10;
    while x do
        begin
            x = x - 1;
            print x;
        end;
end
```

![](https://i.imgur.com/chn69lS.png)

### Ví dụ chứa lỗi
```
begin
    int 3x;
    3x = 41 - 012;
    print x;
end
```

![](https://i.imgur.com/9BVC9Qm.png)
