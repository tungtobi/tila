# Bài tập lớn Tila - Phần Parser 

**Đề bài: Xây dựng trình biên dịch cho ngôn ngữ Tila với văn phạm như file đính kèm.**

## Thông tin học viên
- Họ tên: Nguyễn Tùng Lâm
- MS: 21025095

## Văn phạm
```
Program     -->   begin Statements end EOF
Statements  -->   Statement; Statements
Statement   -->   Decl | Assigment | Loop | print Expr | ϵ
Decl        -->   Type ID 
Type        -->   int
Assigment   -->   Id = Expr
Expr        -->   Expr - Expr 
            |->   Expr * Expr 			 
            |->   Expr ^ Expr 
            |->   ( Expr )
            |->   ID 
            |->   NUMBER
Loop        -->   while Expr do begin Statements end

ID = [a..z]|[A..Z] ([a..z]|[A..Z]|[0..9])*
NUMBER = 0|[1..9][0..9]*

Độ ưu tiên (theo thứ tự giảm dần):
- Expr trong ngoặc 
- ^
- * 
- –

Kết hợp:
- -, *: kết hợp trái
- ^ kết hợp phải
```

## Văn phạm chuyển đổi
```
Program     -->   begin Statements end EOF
Statements  -->   Statement; Statements
Statement   -->   Decl | Assigment | Loop | print Expr | ϵ
Decl        -->   Type ID 
Type        -->   int
Assigment   -->   Id = Expr
Expr        -->   E1 E2
E2          -->   - E1 E2 | ϵ
E1          -->   E3 E4
E4          -->   * E3 E4 | ϵ
E3          -->   E5 E6
E6          -->   ^ E5 E6 | ϵ
E5          -->   ( Expr ) | ID | NUMBER
Loop        -->   while Expr do begin Statements end

ID = [a..z]|[A..Z] ([a..z]|[A..Z]|[0..9])*
NUMBER = 0|[1..9][0..9]*
```

## Hướng dẫn sử dụng

```
java -jar release/tila-1.1.0.jar <path-to-tila-src-file>
```

## Ví dụ

```
begin
    int x;
    x = 41 - 10;
    print x;
end
```

![](https://i.imgur.com/cfCrj7b.png)
