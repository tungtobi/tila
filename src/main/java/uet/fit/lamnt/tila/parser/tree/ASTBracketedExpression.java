package uet.fit.lamnt.tila.parser.tree;

public class ASTBracketedExpression extends ASTExpression implements IASTBracketedExpression {

    private IASTExpression operand;

    @Override
    public IASTExpression getOperand() {
        return operand;
    }

    @Override
    public void setOperand(IASTExpression operand) {
        this.operand = operand;
    }
}
