package uet.fit.lamnt.tila.parser.tree;

public interface IASTPrintStatement extends IASTStatement {

    IASTExpression getValue();

    void setValue(IASTExpression value);
}
