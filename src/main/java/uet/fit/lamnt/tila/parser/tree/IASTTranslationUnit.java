package uet.fit.lamnt.tila.parser.tree;

public interface IASTTranslationUnit extends IASTNode {

    char[] toCharArray();

    IASTCompoundStatement getBody();
}
