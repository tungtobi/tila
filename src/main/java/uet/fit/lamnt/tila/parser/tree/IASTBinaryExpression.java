package uet.fit.lamnt.tila.parser.tree;

public interface IASTBinaryExpression extends IASTExpression {

    /**
     * Sets the operator.
     *
     * @param op value to set.
     */
    void setOperator(int op);

    /**
     * Returns the operator.
     *
     * @return int value as operator
     */
    int getOperator();

    /**
     * minus -
     */
    int op_minus = 1;

    /**
     * multiply *
     */
    int op_multiply = 2;

    /**
     * binary Xor ^
     */
    int op_binaryXor = 3;

    /**
     * Get the first operand.
     *
     * @return <code>IASTExpression</code> representing operand 1.
     */
    IASTExpression getOperand1();

    /**
     * Set the first operand.
     *
     * @param expression
     *            <code>IASTExpression</code> value.
     */
    void setOperand1(IASTExpression expression);

    /**
     * Get the second operand.
     *
     * @return <code>IASTExpression</code> representing operand 2.
     */
    IASTExpression getOperand2();

    /**
     * @param expression
     *            <code>IASTExpression</code> value
     */
    void setOperand2(IASTExpression expression);

    void addFirstChild(IASTNode child);
}
