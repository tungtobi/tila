package uet.fit.lamnt.tila.parser.tree;

public interface IASTIdExpression extends IASTExpression {

    /**
     * Returns the name used in the expression.
     *
     * @return the name
     */
    public IASTName getName();

    /**
     * Sets the name to be used in the expression.
     *
     * @param name
     */
    public void setName(IASTName name);

}
