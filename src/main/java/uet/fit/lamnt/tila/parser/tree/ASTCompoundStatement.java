package uet.fit.lamnt.tila.parser.tree;

import java.util.ArrayList;
import java.util.List;

public class ASTCompoundStatement extends ASTStatement implements IASTCompoundStatement {

    private List<IASTStatement> statements;

    @Override
    public List<IASTStatement> getStatements() {
        return statements;
    }

    @Override
    public void addStatement(IASTStatement statement) {
        this.statements.add(statement);
    }
}
