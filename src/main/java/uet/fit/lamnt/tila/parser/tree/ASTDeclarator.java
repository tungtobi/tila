package uet.fit.lamnt.tila.parser.tree;

public class ASTDeclarator extends ASTNode implements IASTDeclarator {

    private IASTName name;

    @Override
    public IASTName getName() {
        return name;
    }

    @Override
    public void setName(IASTName name) {
        this.name = name;
    }

}
