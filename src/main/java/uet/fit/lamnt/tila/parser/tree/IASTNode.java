package uet.fit.lamnt.tila.parser.tree;

import java.util.List;

public interface IASTNode {

    /**
     * Computes a file location for the node. When the node actually resides in a macro-expansion
     * the location of the expansion is returned. In case the node spans multiple files the location
     * will be in a common root file and will contain the appropriate include directives.
     *
     * @return the mapped file location.
     */
    IASTFileLocation getFileLocation();

    void setFileLocation(IASTFileLocation fileLocation);

    /**
     * Returns the parent node of this node in the tree.
     */
    IASTNode getParent();

    /**
     * Returns the children of this node.
     */
    List<IASTNode> getChildren();

    void addChild(IASTNode child);

    void removeChild(IASTNode child);

    void addChild(IASTNode child, int index);

    /**
     * Sets the parent node of this node in the tree.
     *
     * @param node {@code IASTNode}
     */
    void setParent(IASTNode node);

    /**
     * Returns the raw signature of the IASTNode.
     *
     * @return the raw signature of the IASTNode
     */
    String getRawSignature();

    IASTTranslationUnit getTranslationUnit();

    void setTranslationUnit(IASTTranslationUnit tu);
}
