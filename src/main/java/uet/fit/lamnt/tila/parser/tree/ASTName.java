package uet.fit.lamnt.tila.parser.tree;

public class ASTName extends ASTNode implements IASTName {

    private String name;

    public ASTName(String name) {
        this.name = name;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + name;
    }
}
