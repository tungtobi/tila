package uet.fit.lamnt.tila.parser.tree;

public interface IASTDeclarator extends IASTNode {
    /**
     * Returns the name of the declarator. If this is an abstract
     * declarator, this will return an empty name.
     *
     * @return the name of the declarator
     */
    public IASTName getName();

    /**
     * Sets the name of he declarator.
     *
     * @param name
     *            <code>IASTName</code>
     */
    public void setName(IASTName name);
}
