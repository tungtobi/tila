package uet.fit.lamnt.tila.parser.tree;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class ASTNode implements IASTNode {

    private IASTFileLocation fileLocation;

    private IASTNode parent;

    private final List<IASTNode> children = new ArrayList<>();

    private IASTTranslationUnit tu;

    @Override
    public String getRawSignature() {
        if (tu == null || fileLocation == null || tu.toCharArray() == null)
            return null;

        int startOffset = fileLocation.getNodeOffset();
        int endOffset = startOffset + fileLocation.getNodeLength();

        char[] rawContent = Arrays.copyOfRange(tu.toCharArray(), startOffset, endOffset);
        return new String(rawContent);
    }

    @Override
    public IASTFileLocation getFileLocation() {
        return fileLocation;
    }

    @Override
    public void setFileLocation(IASTFileLocation fileLocation) {
        this.fileLocation = fileLocation;
    }

    @Override
    public IASTNode getParent() {
        return parent;
    }

    @Override
    public void setParent(IASTNode parent) {
        this.parent = parent;
    }

    @Override
    public List<IASTNode> getChildren() {
        return Collections.unmodifiableList(children);
    }

    @Override
    public void addChild(IASTNode child) {
        this.children.add(child);
    }

    @Override
    public void removeChild(IASTNode child) {
        this.children.remove(child);
    }

    @Override
    public void addChild(IASTNode child, int index) {
        this.children.add(index, child);
    }

    @Override
    public IASTTranslationUnit getTranslationUnit() {
        return tu;
    }

    @Override
    public void setTranslationUnit(IASTTranslationUnit tu) {
        this.tu = tu;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName();
    }
}
