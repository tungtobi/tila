package uet.fit.lamnt.tila.parser.tree;

public class ASTPrintStatement extends ASTStatement implements IASTPrintStatement {

    private IASTExpression value;

    @Override
    public IASTExpression getValue() {
        return value;
    }

    @Override
    public void setValue(IASTExpression value) {
        this.value = value;
    }
}
