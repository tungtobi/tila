package uet.fit.lamnt.tila.parser.tree;

public class ASTTranslationUnit extends ASTNode implements IASTTranslationUnit {

    private final char[] content;

    private IASTCompoundStatement body;

    public ASTTranslationUnit(char[] content, String path) {
        this.content = content;
        ASTFileLocation fileLocation = new ASTFileLocation(0, content.length, path);
        setFileLocation(fileLocation);
        setTranslationUnit(this);
    }

    @Override
    public IASTCompoundStatement getBody() {
        return body;
    }

    @Override
    public char[] toCharArray() {
        return content;
    }

    @Override
    public String toString() {
        return super.toString() + ": " + getFileLocation().getPath();
    }
}
