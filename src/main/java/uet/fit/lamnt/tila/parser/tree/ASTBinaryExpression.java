package uet.fit.lamnt.tila.parser.tree;

public class ASTBinaryExpression extends ASTExpression implements IASTBinaryExpression {

    private int operator;

    private IASTExpression operand1, operand2;

    @Override
    public int getOperator() {
        return operator;
    }

    @Override
    public void setOperator(int operator) {
        this.operator = operator;
    }

    @Override
    public IASTExpression getOperand1() {
        return operand1;
    }

    @Override
    public void setOperand1(IASTExpression operand1) {
        this.operand1 = operand1;
    }

    @Override
    public IASTExpression getOperand2() {
        return operand2;
    }

    @Override
    public void setOperand2(IASTExpression operand2) {
        this.operand2 = operand2;
    }

    @Override
    public void addFirstChild(IASTNode child) {
        addChild(child, 0);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": "
                + (operator == 1
                    ? "-"
                        : (operator == 2
                            ? "*"
                                : (operator == 3 ? "^" : "n/a")));
    }
}
