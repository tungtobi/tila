package uet.fit.lamnt.tila.parser.tree;

public interface IASTName extends IASTNode {

    String getName();

    void setName(String name);
}
