package uet.fit.lamnt.tila.parser.tree;

public class ASTDeclSpecifier extends ASTNode implements IASTDeclSpecifier {
    @Override
    public String toString() {
        return getClass().getSimpleName() + ": int";
    }
}
