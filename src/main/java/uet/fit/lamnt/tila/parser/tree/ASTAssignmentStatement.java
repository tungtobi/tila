package uet.fit.lamnt.tila.parser.tree;

public class ASTAssignmentStatement extends ASTStatement implements IASTAssignmentStatement {

    private IASTName assignor;

    private IASTExpression value;

    @Override
    public IASTName getAssignor() {
        return assignor;
    }

    @Override
    public void setAssignor(IASTName assignor) {
        this.assignor = assignor;
    }

    @Override
    public IASTExpression getValue() {
        return value;
    }

    @Override
    public void setValue(IASTExpression value) {
        this.value = value;
    }


}
