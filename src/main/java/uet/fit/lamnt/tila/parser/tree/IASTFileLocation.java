package uet.fit.lamnt.tila.parser.tree;

public interface IASTFileLocation {
    /**
     * Returns the offset within the file where this location starts.
     */
    int getNodeOffset();

    /**
     * Returns the length of this location in terms of characters.
     */
    int getNodeLength();

    String getPath();
}
