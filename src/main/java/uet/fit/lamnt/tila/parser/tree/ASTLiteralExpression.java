package uet.fit.lamnt.tila.parser.tree;

public class ASTLiteralExpression extends ASTExpression implements IASTLiteralExpression {

    private String value;

    @Override
    public String getValue() {
        return value;
    }

    @Override
    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + value;
    }
}
