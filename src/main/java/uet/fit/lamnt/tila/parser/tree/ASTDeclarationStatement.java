package uet.fit.lamnt.tila.parser.tree;

public class ASTDeclarationStatement extends ASTStatement implements IASTDeclarationStatement {

    private IASTDeclarator declarator;

    private IASTDeclSpecifier declSpecifier;

    @Override
    public IASTDeclarator getDeclarator() {
        return declarator;
    }

    @Override
    public void setDeclarator(IASTDeclarator declarator) {
        this.declarator = declarator;
    }

    @Override
    public IASTDeclSpecifier getDeclSpecifier() {
        return declSpecifier;
    }

    @Override
    public void setDeclSpecifier(IASTDeclSpecifier declSpecifier) {
        this.declSpecifier = declSpecifier;
    }
}
