package uet.fit.lamnt.tila.parser.tree;

public class ASTInvalidNameProblemNode extends ASTProblemNode {

    public ASTInvalidNameProblemNode(String name) {
        super("Invalid id " + name);
    }
}
