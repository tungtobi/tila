package uet.fit.lamnt.tila.parser.tree;

public interface IASTAssignmentStatement extends IASTStatement {

    IASTName getAssignor();

    void setAssignor(IASTName assignor);

    IASTExpression getValue();

    void setValue(IASTExpression value);
}
