package uet.fit.lamnt.tila.parser.tree;

public class ASTMissingProblemNode extends ASTProblemNode {

    public ASTMissingProblemNode(Class<? extends IASTNode> cls) {
        super("Missing " + cls.getSimpleName());
    }
}
