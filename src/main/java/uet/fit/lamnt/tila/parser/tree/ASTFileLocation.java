package uet.fit.lamnt.tila.parser.tree;

public class ASTFileLocation implements IASTFileLocation {

    private int nodeOffset, nodeLength;

    private String path;

    public ASTFileLocation(int nodeOffset, int nodeLength, String path) {
        this.nodeOffset = nodeOffset;
        this.nodeLength = nodeLength;
        this.path = path;
    }

    @Override
    public int getNodeOffset() {
        return nodeOffset;
    }

    public void setNodeOffset(int nodeOffset) {
        this.nodeOffset = nodeOffset;
    }

    @Override
    public int getNodeLength() {
        return nodeLength;
    }

    public void setNodeLength(int nodeLength) {
        this.nodeLength = nodeLength;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
