package uet.fit.lamnt.tila.parser.tree;

public interface IASTLiteralExpression extends IASTExpression {

    String getValue();

    void setValue(String value);
}
