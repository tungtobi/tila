package uet.fit.lamnt.tila.parser.tree;

public abstract class ASTProblemNode extends ASTNode implements IASTProblemNode {

    private final String message;

    protected ASTProblemNode(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + ": " + message;
    }
}
