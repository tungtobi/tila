package uet.fit.lamnt.tila.parser.tree;

public class ASTUnexpectedSymbolProblemNode extends ASTProblemNode {

    public ASTUnexpectedSymbolProblemNode(String name) {
        super("Unexpected symbol " + name);
    }
}
