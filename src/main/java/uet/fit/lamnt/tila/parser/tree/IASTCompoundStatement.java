package uet.fit.lamnt.tila.parser.tree;

import java.util.List;

public interface IASTCompoundStatement extends IASTStatement {

    /**
     * Returns the statements in this block.
     *
     * @return Array of IASTStatement
     */
    List<IASTStatement> getStatements();

    /**
     * Adds a statement to the compound block.
     *
     * @param statement the statement to be added
     */
    void addStatement(IASTStatement statement);

}
