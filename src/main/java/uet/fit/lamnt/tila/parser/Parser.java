package uet.fit.lamnt.tila.parser;

import uet.fit.lamnt.tila.parser.tree.*;
import uet.fit.lamnt.tila.scanner.IToken;
import uet.fit.lamnt.tila.scanner.TokenType;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class Parser {

    private final IToken[] tokens;

    private final ParseStack stack;

    private final IASTTranslationUnit tu;

    private int tokenIndex = -1;

    private IToken word;

    private IASTNode focus;

    public Parser(String path, char[] content, IToken[] tokens) {
        this.tu = new ASTTranslationUnit(content, path);
        this.tokens = tokens;
        this.stack = new ParseStack();
    }

    public void parse() {
        IASTTranslationUnit root = tu;
        focus = root;

        IASTNode nullNode = new NullNode();
        stack.push(nullNode);

        word = nextToken();

        while (true) {
            if (focus instanceof EmptyNode) {
                focus = stack.pop();
            } else if (!isTerminal(focus)) {
                // pick next rule to expand focus (A → β1,β2,...,βn)
                IASTNode[] children = null;
                if (focus instanceof IASTTranslationUnit || focus instanceof WhileBodyNode) {
                    children = program();
                } else if (focus instanceof IASTCompoundStatement || focus instanceof StatementsHolder) {
                    children = block();
                } else if (focus instanceof IASTDeclarationStatement) {
                    children = declaration();
                } else if (focus instanceof IASTDeclarator || focus instanceof IASTIdExpression) {
                    children = name();
                } else if (focus instanceof IASTAssignmentStatement) {
                    children = assignment();
                } else if (focus instanceof IASTEmptyStatementNode) {
                    children = new IASTNode[]{new EmptyNode(), new SemicolonNode()};
                } else if (focus instanceof IASTPrintStatement) {
                    children = print();
                } else if (focus instanceof IASTWhileStatement) {
                    children = whileDo();
                } else if (focus instanceof ExpressionHolder) {
                    children = expression(0);
                } else if (focus instanceof SubExpressionHolder) {
                    children = expression(((SubExpressionHolder) focus).level);
                } else if (focus instanceof IASTBracketedExpression) {
                    children = expression(0);
                }

                if (children == null)
                    throw new NullPointerException("Not support " + focus);

                // build nodes for β1 , β2 . . . βn as children of focus
                expandTree(children);

                // push into stack
                for (int i = children.length - 1; i > 0; i--) {
                    stack.push(children[i]);
                }

                focus = children[0];
            } else if (isMatch(word, focus)) {
                // set file location
                int offset = word.getOffset();
                int length = word.getLength();
                String path = tu.getFileLocation().getPath();
                IASTFileLocation fileLocation = new ASTFileLocation(offset, length, path);
                focus.setFileLocation(fileLocation);
                focus.setTranslationUnit(tu);

                if (focus instanceof IASTBinaryExpression) {
                    IASTBinaryExpression binaryExpr = (IASTBinaryExpression) focus;
                    IASTNode parent = focus.getParent();
                    if (parent instanceof IASTBinaryExpression
                            && binaryExpr.getOperator() != IASTBinaryExpression.op_binaryXor
                            && binaryExpr.getOperator() == ((IASTBinaryExpression) parent).getOperator()) {
                        IASTNode grandParent = parent.getParent();
                        parent.removeChild(focus);
                        grandParent.removeChild(parent);
                        parent.setParent(focus);
                        binaryExpr.addFirstChild(parent);
                        focus.setParent(grandParent);
                        grandParent.addChild(focus);
                    } else if (parent != null) {
                        List<IASTNode> children = parent.getChildren();
                        int index = children.indexOf(focus) - 1;
                        if (index >= 0) {
                            IASTNode operand1 = children.get(index);
                            parent.removeChild(operand1);
                            operand1.setParent(focus);
                            binaryExpr.addFirstChild(operand1);
                        }
                    }
                }

                word = nextToken();
                focus = stack.pop();
            } else if (word.getType() == TokenType.EOF && focus instanceof NullNode) {
                break;
            } else {
                System.out.println("Error " + word);
                handleError();
            }
        }

        log(tu, 0);
    }

    private void expandTree(IASTNode[] children) {
        for (IASTNode child : children) {
            if (children.length > 1 && child != children[0]
                    && children[0] instanceof IASTBinaryExpression) {
                IASTNode binExpr = children[0];
                binExpr.addChild(child);
                child.setParent(binExpr);
            } else if (child instanceof ILeafNode && child instanceof TemporaryNode) {
                // do nothing
                child.setParent(focus);
            } else if (focus instanceof TemporaryNode) {
                IASTNode parent = focus.getParent();
                parent.addChild(child);
                child.setParent(parent);
            } else {
                focus.addChild(child);
                child.setParent(focus);
            }
        }

        if (focus instanceof TemporaryNode && focus.getChildren().isEmpty()) {
            IASTNode parent = focus.getParent();
            if (parent != null) {
                parent.removeChild(focus);
            }
        }
    }

    private void handleError() {
        if (focus instanceof CloseBracket) {
            IASTNode peek = stack.peek();
            IASTNode parent = tu;
            if (peek instanceof NullNode) {
                // do nothing
            } else {
                parent = peek;
                while (true) {
                    if (!(parent instanceof TemporaryNode))
                        break;
                    else
                        parent = parent.getParent();
                }
            }
            IASTProblemNode problemNode = new ASTMissingProblemNode(CloseBracket.class);
            problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), 1, ""));
            parent.addChild(problemNode);
            problemNode.setParent(parent);
            focus = stack.pop();
        } else if (focus instanceof EndNode) {
            IASTNode peek = stack.peek();
            IASTNode parent = tu;
            if (peek instanceof NullNode) {
                // do nothing
            } else {
                for (IASTNode child : peek.getParent().getChildren()) {
                    if (child instanceof IASTWhileStatement) {
                        parent = child;
                    }
                }
            }

            for (IASTNode child : tu.getChildren()) {
                if (child instanceof IASTCompoundStatement)
                    parent = child;
            }

            IASTProblemNode problemNode = new ASTMissingProblemNode(EndNode.class);
            problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), 1, ""));
            parent.addChild(problemNode);
            problemNode.setParent(parent);
            focus = stack.pop();
        } else if (focus instanceof ASTStatementProblemNode) {
            while (true) {
                word = nextToken();
                if (word == null)
                    break;

                if (word.getType() == TokenType.SEMICOLON) {
                    word = nextToken();
                    focus = stack.pop();
                    break;
                }
            }
        } else if (focus instanceof ASTMissingProblemNode) {
            focus = stack.pop();
        } else if (focus instanceof IASTProblemNode) {
            word = nextToken();
            focus = stack.pop();
        } else {
            IASTProblemNode problemNode = new ASTUnexpectedSymbolProblemNode(word.getImage());
            problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), 1, ""));
            IASTNode parent = focus;
            while (true) {
                if (parent instanceof NullNode)
                    parent = tu;
                else if ((parent instanceof TemporaryNode))
                    parent = parent.getParent();
                else
                    break;
            }
            parent.addChild(problemNode);
            problemNode.setParent(parent);
            word = nextToken();
        }
    }

    private IASTNode[] whileDo() {
        return new IASTNode[]{
                new WhileNode(),
                new ExpressionHolder(),
                new DoNode(),
                new WhileBodyNode()
        };
    }

    private IASTNode[] print() {
        return new IASTNode[]{
                new PrintNode(),
                new ExpressionHolder(),
                new SemicolonNode()
        };
    }

    private IASTNode[] expression(int level) {
        List<IASTNode> nodes = new ArrayList<>();
        switch (level) {
            case 0:
                nodes.add(new SubExpressionHolder(1));
                nodes.add(new SubExpressionHolder(2));
                break;

            case 1:
                nodes.add(new SubExpressionHolder(3));
                nodes.add(new SubExpressionHolder(4));
                break;

            case 3:
                nodes.add(new SubExpressionHolder(5));
                nodes.add(new SubExpressionHolder(6));
                break;

            case 2:
                if (word.getType() == TokenType.OPERATOR
                        && word.getImage().equals("-")) {
                    IASTBinaryExpression expression = new ASTBinaryExpression();
                    expression.setOperator(IASTBinaryExpression.op_minus);
                    nodes.add(expression);

                    nodes.add(new SubExpressionHolder(1));
                    nodes.add(new SubExpressionHolder(2));
                } else if (isNotTerminateExpr(word)) {
                    IASTProblemNode problemNode = new ASTUnexpectedSymbolProblemNode(word.getImage());
                    problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), word.getLength(), ""));
                    nodes.add(problemNode);
                    nodes.add(new ExpressionHolder());
                } else {
                    nodes.add(new EmptyNode());
                }

                break;

            case 4:
                if (word.getType() == TokenType.OPERATOR
                        && word.getImage().equals("*")) {
                    IASTBinaryExpression expression = new ASTBinaryExpression();
                    expression.setOperator(IASTBinaryExpression.op_multiply);
                    nodes.add(expression);

                    nodes.add(new SubExpressionHolder(3));
                    nodes.add(new SubExpressionHolder(4));
                } else if (isNotTerminateExpr(word)) {
                    IASTProblemNode problemNode = new ASTUnexpectedSymbolProblemNode(word.getImage());
                    problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), word.getLength(), ""));
                    nodes.add(problemNode);
                    nodes.add(new ExpressionHolder());
                } else {
                    nodes.add(new EmptyNode());
                }

                break;

            case 6:
                if (word.getType() == TokenType.OPERATOR
                        && word.getImage().equals("^")) {
                    IASTBinaryExpression expression = new ASTBinaryExpression();
                    expression.setOperator(IASTBinaryExpression.op_binaryXor);
                    nodes.add(expression);

                    nodes.add(new SubExpressionHolder(5));
                    nodes.add(new SubExpressionHolder(6));
                } else if (isNotTerminateExpr(word)) {
                    IASTProblemNode problemNode = new ASTUnexpectedSymbolProblemNode(word.getImage());
                    problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), word.getLength(), ""));
                    nodes.add(problemNode);
                    nodes.add(new ExpressionHolder());
                } else {
                    nodes.add(new EmptyNode());
                }

                break;

            case 5:
                switch (word.getType()) {
                    case ID:
                        IASTIdExpression idExpression = new ASTIdExpression();
                        nodes.add(idExpression);
                        break;

                    case NUMBER:
                        IASTLiteralExpression literalExpression = new ASTLiteralExpression();
                        literalExpression.setValue(word.getImage());
                        nodes.add(literalExpression);
                        break;

                    case OPEN_BRACKET:
                        IASTBracketedExpression expression = new ASTBracketedExpression();
                        nodes.add(new OpenBracket());
                        nodes.add(expression);
                        nodes.add(new CloseBracket());
                        break;
                        
                    case ERROR:
                    case BEGIN:
                    case END:
                    case WHILE:
                    case DO:
                    case PRINT:
                    case INT: {
                        IASTProblemNode problemNode = new ASTInvalidNameProblemNode(word.getImage());
                        problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), word.getLength(), ""));
                        nodes.add(problemNode);
                        break;
                    }
                        
                    default: {
                        IASTProblemNode problemNode = new ASTMissingProblemNode(IASTExpression.class);
                        problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), 1, ""));
                        nodes.add(problemNode);
                        break;
                    }
                }
        }

        return nodes.toArray(new IASTNode[0]);
    }

    private boolean isNotTerminateExpr(IToken word) {
        return word.getType() != TokenType.SEMICOLON
                && word.getType() != TokenType.CLOSE_BRACKET
                && word.getType() != TokenType.DO
                && word.getType() != TokenType.OPERATOR;
    }

    private IASTNode[] assignment() {
        return new IASTNode[]{
                toName(word),
                new AssignNode(),
                new ExpressionHolder(),
                new SemicolonNode()
        };
    }

    private IASTNode[] name() {
        return new IASTNode[]{toName(word)};
    }

    private IASTNode toName(IToken word) {
        if (word.getType() == TokenType.ID) {
            return new ASTName(word.getImage());
        } else {
            IASTProblemNode problemNode = new ASTInvalidNameProblemNode(word.getImage());
            problemNode.setFileLocation(new ASTFileLocation(word.getOffset(), word.getLength(), ""));
            return problemNode;
        }
    }

    private IASTNode[] declaration() {
        return new IASTNode[]{
                new ASTDeclSpecifier(),
                new ASTDeclarator(),
                new SemicolonNode()
        };
    }

    private IASTNode[] program() {
        return new IASTNode[]{
                new BeginNode(),
                new ASTCompoundStatement(),
                new EndNode()
        };
    }

    private IASTNode[] block() {
        IASTNode statement;
        switch (word.getType()) {
            case INT:
                statement = new ASTDeclarationStatement();
                break;

            case PRINT:
                statement = new ASTPrintStatement();
                break;

            case WHILE:
                statement = new ASTWhileStatement();
                break;

            case SEMICOLON:
                statement = new ASTEmptyStatementNode();
                break;

            case END:
            case EOF:
                statement = new EmptyNode();
                break;

            case ID:
                IToken next = lookahead();
                if (next != null && next.getType() == TokenType.ASSIGN)
                    statement = new ASTAssignmentStatement();
                else
                    statement = new ASTStatementProblemNode();
                break;

            default:
                statement = new ASTStatementProblemNode();
        }

        return statement instanceof EmptyNode ?
                new IASTNode[]{statement} :
                new IASTNode[]{statement, new StatementsHolder()};
    }

    private boolean isTerminal(IASTNode node) {
        return node instanceof IASTDeclSpecifier
                || node instanceof IASTProblemNode
                || node instanceof IASTName
                || node instanceof IASTBinaryExpression
                || node instanceof NullNode
                || node instanceof ILeafNode
                || node instanceof IASTLiteralExpression;
    }

    private boolean isMatch(IToken word, IASTNode focus) {
        if (focus instanceof IASTDeclSpecifier) {
            return word.getType() == TokenType.INT;
        } else if (focus instanceof PrintNode) {
            return word.getType() == TokenType.PRINT;
        } else if (focus instanceof WhileNode) {
            return word.getType() == TokenType.WHILE;
        } else if (focus instanceof DoNode) {
            return word.getType() == TokenType.DO;
        } else if (focus instanceof BeginNode) {
            return word.getType() == TokenType.BEGIN;
        } else if (focus instanceof EndNode) {
            return word.getType() == TokenType.END;
        } else if (focus instanceof SemicolonNode) {
            return word.getType() == TokenType.SEMICOLON;
        } else if (focus instanceof AssignNode) {
            return word.getType() == TokenType.ASSIGN;
        } else if (focus instanceof IASTBinaryExpression) {
            IASTBinaryExpression expression = (IASTBinaryExpression) focus;
            String operator;
            switch (expression.getOperator()) {
                case IASTBinaryExpression.op_minus:
                    operator = "-";
                    break;

                case IASTBinaryExpression.op_multiply:
                    operator = "*";
                    break;

                case IASTBinaryExpression.op_binaryXor:
                    operator = "^";
                    break;

                default:
                    operator = null;
                    break;
            }

            return word.getImage().equals(operator);
        } else if (focus instanceof CloseBracket) {
            return word.getType() == TokenType.CLOSE_BRACKET;
        } else if (focus instanceof OpenBracket) {
            return word.getType() == TokenType.OPEN_BRACKET;
        } else if (focus instanceof IASTLiteralExpression) {
            String tokenImage = word.getImage();
            String id = ((IASTLiteralExpression) focus).getValue();
            return word.getType() == TokenType.NUMBER && tokenImage.equals(id);
        } else if (focus instanceof IASTName) {
            String tokenImage = word.getImage();
            String id = ((IASTName) focus).getName();
            return word.getType() == TokenType.ID && tokenImage.equals(id);
        } else
            return false;
    }

    private IToken nextToken() {
        tokenIndex++;
        if (tokenIndex < tokens.length && tokenIndex >= 0)
            return tokens[tokenIndex];
        else
            return null;
    }

    private IToken lookahead() {
        int tokenIndex = this.tokenIndex + 1;
        if (tokenIndex < tokens.length && tokenIndex >= 0)
            return tokens[tokenIndex];
        else
            return null;
    }

    private void log(IASTNode node, int level) {
        for (int i = 0; i < level * 4; i++) {
            if (i == 0) System.out.print("|");
            else System.out.print("-");
        }
        System.out.println(node);
        for (IASTNode child : node.getChildren())
            log(child, level + 1);
    }

    private interface ILeafNode {
    }

    private static class SubExpressionHolder extends TemporaryNode {

        private final int level;

        public SubExpressionHolder(int level) {
            this.level = level;
        }
    }

    private static class WhileBodyNode extends TemporaryNode {

    }

    private static class WhileNode extends TemporaryNode implements ILeafNode {
    }

    private static class DoNode extends TemporaryNode implements ILeafNode {
    }

    private static class PrintNode extends TemporaryNode implements ILeafNode {
    }

    private static class OpenBracket extends TemporaryNode implements ILeafNode {
    }

    private static class AssignNode extends TemporaryNode implements ILeafNode {
    }

    private static class CloseBracket extends TemporaryNode implements ILeafNode {
    }

    private static class ExpressionHolder extends TemporaryNode {
    }

    private static class SemicolonNode extends TemporaryNode implements ILeafNode {
    }

    private static class StatementsHolder extends TemporaryNode {
    }

    private static class BeginNode extends TemporaryNode implements ILeafNode {
    }

    private static class EndNode extends TemporaryNode implements ILeafNode {
    }

    private static class EmptyNode extends TemporaryNode implements ILeafNode {
    }

    private static class NullNode extends TemporaryNode {
    }

    private static abstract class TemporaryNode extends ASTNode {
    }

    private static class ParseStack extends Stack<IASTNode> {

    }
}
