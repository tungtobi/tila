package uet.fit.lamnt.tila.parser.tree;

public interface IASTBracketedExpression extends IASTExpression {

    /**
     * Returns the operand.
     *
     * @return {@code IASTExpression}
     */
    IASTExpression getOperand();

    /**
     * Sets the operand.
     *
     * @param expression {@code IASTExpression}
     */
    void setOperand(IASTExpression expression);
}
