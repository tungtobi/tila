package uet.fit.lamnt.tila.parser.tree;

public interface IASTWhileStatement extends IASTStatement {

    /**
     * Returns the condition on the while loop
     *
     * @return expression for the condition
     */
    IASTExpression getCondition();

    /**
     * Sets the condition of the while loop.
     *
     * @param condition
     */
    void setCondition(IASTExpression condition);

    /**
     * The body of the loop.
     *
     * @return the body
     */
    IASTCompoundStatement getBody();

    /**
     * Sets the body of the while loop.
     *
     * @param body
     */
    void setBody(IASTCompoundStatement body);
}
