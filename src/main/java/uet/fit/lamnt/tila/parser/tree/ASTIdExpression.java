package uet.fit.lamnt.tila.parser.tree;

public class ASTIdExpression extends ASTExpression implements IASTIdExpression {

    private IASTName name;

    @Override
    public IASTName getName() {
        return name;
    }

    @Override
    public void setName(IASTName name) {
        this.name = name;
    }
}
