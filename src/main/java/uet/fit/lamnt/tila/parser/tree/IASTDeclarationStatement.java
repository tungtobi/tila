package uet.fit.lamnt.tila.parser.tree;

public interface IASTDeclarationStatement extends IASTStatement {

    /**
     * This returns the object representing the declSpecifiers for this
     * declaration.
     *
     * @return the declSpecifier object
     */
    public IASTDeclSpecifier getDeclSpecifier();

    /**
     * Set the decl specifier.
     *
     * @param declSpec
     *            <code>IASTDeclSpecifier</code>
     */
    public void setDeclSpecifier(IASTDeclSpecifier declSpec);

    /**
     * This returns the list of declarators in this declaration.
     *
     * @return <code>IASTDeclarator []</code>
     */
    public IASTDeclarator getDeclarator();

    /**
     * Add a declarator.
     *
     * @param declarator
     *            <code>IASTDeclarator</code>
     */
    public void setDeclarator(IASTDeclarator declarator);

}
