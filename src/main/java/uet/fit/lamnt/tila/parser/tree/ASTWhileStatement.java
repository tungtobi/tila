package uet.fit.lamnt.tila.parser.tree;

public class ASTWhileStatement extends ASTStatement implements IASTWhileStatement {

    private IASTExpression condition;

    private IASTCompoundStatement body;

    @Override
    public IASTExpression getCondition() {
        return condition;
    }

    @Override
    public void setCondition(IASTExpression condition) {
        this.condition = condition;
    }

    @Override
    public IASTCompoundStatement getBody() {
        return body;
    }

    @Override
    public void setBody(IASTCompoundStatement body) {
        this.body = body;
    }
}
