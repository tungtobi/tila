package uet.fit.lamnt.tila.parser.tree;

public class ASTStatementProblemNode extends ASTProblemNode {

    public ASTStatementProblemNode() {
        super("Invalid statement");
    }
}
