package uet.fit.lamnt.tila;

import uet.fit.lamnt.tila.parser.Parser;
import uet.fit.lamnt.tila.scanner.IToken;
import uet.fit.lamnt.tila.scanner.Scanner;
import uet.fit.lamnt.tila.scanner.TokenType;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {
        if (args.length != 1)
            throw new Exception("Invalid argument format");

        File file = new File(args[0]);
        if (!file.isFile() || !file.exists())
            throw new Exception("Invalid tila source code file");

        Scanner scanner = new Scanner();
        scanner.setSourceCode(file);
        IToken[] tokens = scanner.scan();
        char[] content = scanner.getContent();

        Parser parser = new Parser(file.getAbsolutePath(), content, tokens);
        parser.parse();
    }
}
