package uet.fit.lamnt.tila.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FileUtils {

    public static String read(File file) {
        StringBuilder fileData = new StringBuilder();
        try {
            BufferedReader reader;
            reader = new BufferedReader(new FileReader(file));
            char[] buf = new char[10];
            int numRead;
            while ((numRead = reader.read(buf)) != -1) {
                String readData = String.valueOf(buf, 0, numRead);
                fileData.append(readData);
                buf = new char[1024];
            }
            reader.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return fileData.toString();
        }
    }
}
