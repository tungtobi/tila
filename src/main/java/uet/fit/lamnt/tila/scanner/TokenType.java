package uet.fit.lamnt.tila.scanner;

public enum TokenType {
    UNKNOWN,
    ERROR,
    BEGIN,
    END,
    INT,
    PRINT,
    WHILE,
    DO,
    ID,
    NUMBER,
    OPERATOR,
    ASSIGN,
    OPEN_BRACKET,
    CLOSE_BRACKET,
    SEMICOLON,
    EOF
}