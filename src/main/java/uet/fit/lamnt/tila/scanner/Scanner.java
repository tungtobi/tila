package uet.fit.lamnt.tila.scanner;

import uet.fit.lamnt.tila.util.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class Scanner implements IScanner {

    private char[] content;
    private int offset = 0;

    public IToken[] scan() {
        List<IToken> tokenList = new ArrayList<>();
        IToken token;
        do {
            token = nextToken();
            tokenList.add(token);
        } while (token.getType() != TokenType.EOF);
        return tokenList.toArray(new IToken[0]);
    }

    @Override
    public IToken nextToken() {
        IToken token;

        String tokenImage = "";

        int startOffset = offset;
        State state = State.SOT;
        TokenType type = TokenType.UNKNOWN;

        while (true) {
            try {
                char c = nextChar();

                if (isAlphabetic(c)) {
                    if (state == State.SOT) {
                        type = TokenType.ID;
                        state = State.IOT;
                    } else if (type != TokenType.ID) {
                        type = TokenType.ERROR;
                    }

                    tokenImage += c;
                    offset++;

                } else if (isDigit(c)) {
                    if (state == State.SOT) {
                        state = State.IOT;
                        type = TokenType.NUMBER;
                    } else if (tokenImage.equals("0")) {
                        type = TokenType.ERROR;
                    }

                    tokenImage += c;
                    offset++;

                } else if (isBlank(c)) {
                    if (state == State.SOT) {
                        // do nothing
                    } else {
                        state = State.EOT;
                    }

                    offset++;

                } else if (isSpecial(c)) {
                    if (state == State.SOT) {
                        if (c == '(')
                            type = TokenType.OPEN_BRACKET;
                        else if (c == ')')
                            type = TokenType.CLOSE_BRACKET;
                        else if (c == '=')
                            type = TokenType.ASSIGN;
                        else if (c == ';')
                            type = TokenType.SEMICOLON;
                        else
                            type = TokenType.OPERATOR;

                        tokenImage += c;
                        offset++;
                    }

                    state = State.EOT;
                } else {
                    state = State.IOT;
                    type = TokenType.ERROR;
                    tokenImage += c;
                    offset++;
                }

                if (state == State.EOT) {
                    if (type == TokenType.ID && isReversedWords(tokenImage))
                        type = toTokenType(tokenImage);
                    token = new Token(type, tokenImage, startOffset);
                    break;
                }

            } catch (IndexOutOfBoundsException ex) {
                if (state == State.IOT) {
                    if (type == TokenType.ID && isReversedWords(tokenImage))
                        type = toTokenType(tokenImage);
                    token = new Token(type, tokenImage, startOffset);
                } else {
                    token = Token.EOF(offset);
                }
                break;
            }
        }

        return token;
    }

    private TokenType toTokenType(String image) {
        return TokenType.valueOf(image.toUpperCase());
    }

    private boolean isReversedWords(String word) {
        for (String rw : REVERSED_WORDS) {
            if (rw.equals(word))
                return true;
        }
        return false;
    }

    public char[] getContent() {
        return content;
    }

    @Override
    public void setSourceCode(File file) {
        String sourceCode = FileUtils.read(file);
        this.content = sourceCode.toCharArray();
    }

    private boolean isDigit(char c) {
        return c >= '0' && c <= '9';
    }

    private boolean isAlphabetic(char c) {
        return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
    }

    private boolean isBlank(char c) {
        return c == ' ' || c == '\n' || c == '\t';
    }

    private boolean isSpecial(char c) {
        for (char sc : SPECIAL_CHARACTERS) {
            if (c == sc)
                return true;
        }
        return false;
    }

    private char nextChar() throws IndexOutOfBoundsException {
        return this.content[this.offset];
    }
}
