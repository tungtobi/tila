package uet.fit.lamnt.tila.scanner;

import java.io.File;

public interface IScanner {

    char[] SPECIAL_CHARACTERS = new char[] { '(', ')', ';', '-', '*', '^', '='};
    String[] REVERSED_WORDS = new String[] {"begin", "end", "int", "print", "while", "do"};

    IToken nextToken();

    IToken[] scan();

    void setSourceCode(File file);

    enum State {
        SOT,
        IOT,
        EOT,
    }
}
