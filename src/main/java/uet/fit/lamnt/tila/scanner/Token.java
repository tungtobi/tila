package uet.fit.lamnt.tila.scanner;

public class Token implements IToken {

    private final TokenType type;
    private final int offset;
    private final String image;

    public Token(TokenType type, String image, int offset) {
        this.type = type;
        this.image = image;
        this.offset = offset;
    }

    public static IToken EOF(int offset) {
        return new Token(TokenType.EOF, "", offset);
    }

    public static IToken ERROR(String image, int offset) {
        return new Token(TokenType.ERROR, image, offset);
    }

    @Override
    public TokenType getType() {
        return type;
    }

    @Override
    public int getOffset() {
        return offset;
    }

    @Override
    public int getLength() {
        return image == null ? -1 : image.length();
    }

    @Override
    public String getImage() {
        return image;
    }

    @Override
    public String toString() {
        return "Token{" + getType() +
                (getType() == TokenType.ERROR
                || getType() == TokenType.NUMBER
                || getType() == TokenType.ID
                || getType() == TokenType.OPERATOR ?
                        " '" + getImage() + '\''  : "") +
                " at " + getOffset() +
                '}';
    }
}
