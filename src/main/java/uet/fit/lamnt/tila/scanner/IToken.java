package uet.fit.lamnt.tila.scanner;

public interface IToken {
    
    TokenType getType();
    
    int getOffset();
    
    int getLength();

    String getImage();
}
