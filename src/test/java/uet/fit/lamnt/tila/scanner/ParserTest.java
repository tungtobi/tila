package uet.fit.lamnt.tila.scanner;

import org.junit.Test;
import uet.fit.lamnt.tila.parser.Parser;

import java.io.File;

public class ParserTest {

    private void parse(File file) {
        Scanner scanner = new Scanner();
        scanner.setSourceCode(file);
        IToken[] tokens = scanner.scan();
        char[] content = scanner.getContent();

        Parser parser = new Parser(file.getAbsolutePath(), content, tokens);
        parser.parse();
    }

    @Test
    public void simpleTest1() {
        File file = new File("datatest/simple1.tila");
        parse(file);
    }

    @Test
    public void simpleTest() {
        parse(new File("datatest/simple.tila"));
    }

    @Test
    public void nameTest1() {
        parse(new File("datatest/name1.tila"));
    }

    @Test
    public void bracketTest1() {
        parse(new File("datatest/banketsimple.tila"));
    }

    @Test
    public void bracketErrorTest1() {
        parse(new File("datatest/banketerror.tila"));
    }

    @Test
    public void bracketErrorTest2() {
        parse(new File("datatest/banketerror2.tila"));
    }

    @Test
    public void loopSimpleTest() {
        parse(new File("datatest/loop.tila"));
    }

    @Test
    public void checkAmstrongTest() {
        parse(new File("datatest/check_amstrong.tila"));
    }

    @Test
    public void checkSubsequenceTest() {
        parse(new File("datatest/check_subsequence.tila"));
    }

    @Test
    public void binaryExprTest() {
        parse(new File("datatest/op_priority.tila"));
    }

    @Test
    public void minusCombTest() {
        parse(new File("datatest/minus_comb.tila"));
    }

    @Test
    public void xorCombTest() {
        parse(new File("datatest/xor_comb.tila"));
    }

    @Test
    public void nestedLoopTest() {
        parse(new File("datatest/nestedloop.tila"));
    }
}