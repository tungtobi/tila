package uet.fit.lamnt.tila.scanner;

import org.junit.Test;

import java.io.File;

public class ScannerTest {

    private void scan(File file) {
        Scanner scanner = new Scanner();
        scanner.setSourceCode(file);
        IToken token;
        do {
            token = scanner.nextToken();
            System.out.println(token);
        } while (token.getType() != TokenType.EOF);
    }

    @Test
    public void simpleTest() {
        scan(new File("datatest/simple.tila"));
    }

    @Test
    public void nameTest1() {
        scan(new File("datatest/name1.tila"));
    }

    @Test
    public void bracketTest1() {
        scan(new File("datatest/banketsimple.tila"));
    }

    @Test
    public void loopSimpleTest() {
        scan(new File("datatest/loop.tila"));
    }
}